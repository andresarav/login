import React from 'react'
import './inputStyles.css'
import { SimpleLoader } from '../loaders'

export const InputForm = (props) => {
const {
  clase,
  focusAction,
  unFocusAction,
  state_item,
  active,
  type,
  placeholder,
  actualizarEstado,
  name,
  value
} = props


  return(
    <div className={`${!clase ? 'containerInputComponent' : clase}`}>
      <p className="labelText fuente" style={{display:!props.label ? 'none' : 'initial' }}>{props.label}</p>
      <div className={`inputContainer ${active ? 'inputActivado' : '' } ${state_item}`}>
        <input
          className={`inputElement ${active ? 'inputActivado' : '' }`}
          type={type}
          placeholder={placeholder}
          onChange={actualizarEstado}
          onFocus={focusAction}
          onBlur={unFocusAction}
          name={name}
          defaultValue={value}
        />
      </div>
    </div>
  )
}



export const InputFormAuth = (props) => {

const {
  clase,
  label,
  active,
  type,
  placeholder,
  actualizarEstado,
  name,
  value,
  status,
  verifying,
  error
  } = props

  return(
    <div className={`${!clase ? 'containerInputComponent AuthInputComp' : clase}`}>
      <p className="labelText fuente" style={{display:!label ? 'none' : 'initial' }}>{label}</p>
      <div
        className="inputContainer inputAuths"
        style={{border:(verifying && !active) ? '1px solid #039aff' : active ? '1px solid #59b200' : error ? '1px solid red' :'1px solid #50667a61'}}
        >
        {
          !verifying ?
          <input
            className={`inputElement`}
            style={{color:active ? '#59b200' : 'gray' }}
            type={type}
            placeholder={placeholder}
            onChange={actualizarEstado}
            name={name}
            defaultValue={value}
          />
          :
          <div className="AuthLoader">
            <SimpleLoader/>
          </div>
        }
      </div>
          <p
            className="statusInput"
            style={{color:(verifying && !active) ? '#039aff' : active ? '#59b200' : error ? 'red' :'#50667a61'}}
            >
              <i className="fas fa-check"
                style={{display: active ? 'initial' :'none'}}
                ></i>
                {status}
            </p>
    </div>
  )
}



export default InputForm
