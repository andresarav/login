import React from 'react'
import './buttons.css'

export const InputButton = (props) => {
  return(
    <div className="InputButton" >
      {
        props.active ?
        <input className={`botonForm ${props.type} fuente `} type="submit" value={props.label} onClick={props.action} />
        :
        <div className="botonForm desactivado fuente" style={{width:props.ancho}}  >
          {props.label}
        </div>
      }
    </div>
  )
}

export default InputButton
