import React, { Component } from 'react';
import AuthComponentContainer from './components/auth';
import './components/auth/authComponent.css'


class App extends Component {
  render() {
    return (
      <AuthComponentContainer/>
    );
  }
}

export default App;
